﻿var fs = require("fs");
var Bot = require("./Bot");
var Database = require("./Database");

var MESSAGE_NO_SCORES = "Ennätyslista on tyhjä!";
var MESSAGE_QUIZ_RUNNING = "Edellinen kysymys on vielä kesken.";

var COMMAND_SCORES = "/scores";
var COMMAND_QUIZ = "/quiz";

function TriviaGame() {
	var self = this;

	this.question = {};
	this.bot = null;
	this.database = null;
	
	fs.readFile("token.txt", "utf8", function (error, data) {
		if(error) {
			console.log(error);
		}
		
		self.bot = new Bot(data);
		self.database = new Database('database.db');
		
		setInterval(function() {self.getUpdates();}, 2000);
	});
}

TriviaGame.prototype.showTopPlayers = function(chatId) {
	var self = this;
	self.database.topPlayers(chatId, function(scores) {
		if(scores.length == 0) {
			self.bot.sendMessage(MESSAGE_NO_SCORES, chatId);
		} 
		else {
			var position = 1;
			var message = new String("*TOP10 pelaajat*\n");
		
			scores.forEach(function (score) {
				message += position + ". " + score.username + " (" + score.score + "p)" +"\n";
				position++;
			});
		
			self.bot.sendMessage(message, chatId);
		}
	});
}

TriviaGame.prototype.askQuestion = function(chatId) {
	var self = this;
	if(self.question[chatId] === undefined || self.question[chatId] === null) {
		self.database.randomQuestion(function(data) {
			var message = new String("*KYSYMYS: " + data.question + "*");
			
			message += "\n*A)* " + data.a;
			message += "\n*B)* " + data.b;
			message += "\n*C)* " + data.c;
			message += "\n*D)* " + data.d;
			
			self.bot.sendMessage(message, chatId);
			self.question[chatId] = data;
		});
	}
	else {
		self.bot.sendMessage(MESSAGE_QUIZ_RUNNING, chatId);
	}
}

TriviaGame.prototype.processAnswer = function(chatId, userId, username, message) {
	var self = this;
	if(self.question[chatId] != null) {
		var trimmed = message.trim().toLowerCase().substring(1);
		var values = "abcd";
		
		if(trimmed.length == 1 && values.includes(trimmed)) {
			if(values.charAt(self.question[chatId].correct-1) == trimmed) {
				self.bot.sendMessage(username + " vastasi oikein! (+1p)", chatId);
				self.database.addPlayerScore(chatId, userId, username, 1);
				self.question[chatId] = null;
			}
			else {
				self.bot.sendMessage(username + " vastasi väärin. (-1p)", chatId);
				self.database.addPlayerScore(chatId, userId, username, -1);
			}
		}
	}
}

TriviaGame.prototype.handleUpdate = function(update) {
	var chatId = update.message.chat.id;
	var message = update.message.text;
	var userId = update.message.from.id;
	var username = update.message.from.first_name;
	
	console.log(message);
	
	if(message != undefined) {
		if(message.trim() === COMMAND_SCORES) {
			this.showTopPlayers(chatId);
		}
		else if(message.trim() === COMMAND_QUIZ) {
			this.askQuestion(chatId);
		}
		else {
			this.processAnswer(chatId, userId, username, message);
		}
	}
}

TriviaGame.prototype.getUpdates = function() {
	var self = this;
	self.bot.getUpdates(function(result) {
		self.handleUpdate(result);
	});
}

module.exports = TriviaGame;





