var https = require("https");

function TelegramBot(token) {
	this.token = token;
	this.offset = 0;
	
	this.options = {
		hostname: "api.telegram.org",
		port: "443",
		method: "GET",
	};
}

TelegramBot.prototype.getUpdates = function (updateHandler) {
	var self = this;
	var path = "/" + self.token + "/" + encodeURI("getUpdates?offset=" + self.offset);
	self.options.path = path;
	
	https.get(self.options, function(res) {
		res.on('data', function(data) {
			var results = JSON.parse(data).result;
			
			results.forEach(function (result) {
				if(result.update_id >= self.offset) {
					self.offset = result.update_id + 1;
				}
				
				updateHandler(result);
			});
		});
	});
}

TelegramBot.prototype.sendMessage = function (text, chatId) {
	var self = this;
	var path =  "/" + self.token + "/" + encodeURI("sendMessage?chat_id=" + chatId + "&text=" + text + "&parse_mode=Markdown");
	self.options.path = path;
	
	https.get(self.options, function(res) {
		res.on('data', function(data) {
			//console.log(JSON.parse(data).result);
		});
	});
}

module.exports = TelegramBot;