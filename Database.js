﻿var sqlite = require('sqlite3').verbose();

var QUERY_TOP_PLAYERS = "SELECT username, score FROM user_score WHERE chat_id = ? ORDER BY score DESC LIMIT 5";
var QUERY_GET_SCORE = "SELECT score FROM user_score WHERE chat_id = ? AND user_id = ?";
var QUERY_INSERT_SCORE = "INSERT INTO user_score (chat_id, user_id, username, score) VALUES (?, ?, ?, ?)";
var QUERY_UPDATE_SCORE = "UPDATE user_score SET score = ?, username = ? WHERE chat_id = ? AND user_id = ?";
var QUERY_GET_QUESTION = "SELECT * FROM question ORDER BY RANDOM() LIMIT 1;";

function Database(file) {
	this.db = new sqlite.Database(file);
}

Database.prototype.topPlayers = function(chatId, handler) {
	var self = this;
	self.db.all(QUERY_TOP_PLAYERS, chatId, function(err, rows) {
		console.log(err);
		handler(rows);
	});
}

Database.prototype.addPlayerScore = function(chatId, userId, username, amount) {
	var self = this;
	self.db.get(QUERY_GET_SCORE, [chatId, userId], function(err, row) {
		console.log(err);
	
		if(row === undefined) {
			self.db.run(QUERY_INSERT_SCORE, [chatId, userId, username, amount]);
		}
		else {
			self.db.run(QUERY_UPDATE_SCORE, [row.score + amount, username, chatId, userId]);
		}
	});
}

Database.prototype.randomQuestion = function(handler) {
	var self = this;
	self.db.get(QUERY_GET_QUESTION, [], function(err, row) {
		console.log(err);
		handler(row);
	});
}

module.exports = Database;